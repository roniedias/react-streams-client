import _ from 'lodash';

import {
    FETCH_STREAM,
    FETCH_STREAMS,
    CREATE_STREAM,
    EDIT_STREAM,
    DELETE_STREAM
} from '../actions/types'

export default (state = {}, action) => {
    switch (action.type) {
        case FETCH_STREAMS:
            return { ...state, ..._.mapKeys(action.payload, 'id') };
        case FETCH_STREAM:
            return { ...state, [action.payload.id]: action.payload }; // action.payload will be the one (with correct id) returned by the action
        case CREATE_STREAM:
            return { ...state, [action.payload.id]: action.payload };
        case EDIT_STREAM:
            return { ...state, [action.payload.id]: action.payload };
        case DELETE_STREAM:
            return _.omit(state, action.payload); // We call omit passing the state object as the first argument and a string of the key that we want to remove as the second.
                                                  // When we dispatch an action of type DELETE_STREAM, the payload is the id itself
                                                  // Also, omit does not change the original object. Instead it creates a new one

        default: 
            return state;
    }
}