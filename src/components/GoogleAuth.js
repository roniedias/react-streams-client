import React from 'react';
import { connect } from 'react-redux';
import { signIn, signOut } from '../actions';

class GoogleAuth extends React.Component {
    componentDidMount() {
        
        // 1st argument (client:auth2): for React App to know that this variable is available on window scope.
        // Allows us to get a "signal" or notification of window loading process is complete, by passing 2nd 
        // argument

        // 2nd argument: Returns a primise, an object that give us a little "tap on the 
        // shoulder" (notification) after this client library have been successfully initialized 
        window.gapi.load('client:auth2', () => {
            window.gapi.client.init({ 
                clientId: '712513626717-78j127q2a3o460bbpbr88dr71fjgiu2i.apps.googleusercontent.com',
                scope: 'email'
            }).then( () => { // Will be automatically invoked after our library is successfully initialized itself
                             // Anything inside this block will be executed only after our entire gapi lib is ready to go
                this.auth = window.gapi.auth2.getAuthInstance();
                this.onAuthChange(this.auth.isSignedIn.get());
                this.auth.isSignedIn.listen(this.onAuthChange);
            });
        }); 
    }

    onAuthChange = (isSignedIn) => { // Will be called any time the user's authentication status changes
        if(isSignedIn) {
            this.props.signIn(this.auth.currentUser.get().getId()); // Action Creator
        }
        else {
            this.props.signOut();  // Action Creator
        }
    };

    onSignInClick = () => {
        this.auth.signIn();
    };

    onSignOutClick = () => {
        this.auth.signOut();
    };
   

    renderAuthButton() {
        if(this.props.isSignedIn === null) {
            return null;
        } 
        else if (this.props.isSignedIn) {
            return (
                <button onClick={this.onSignOutClick} className="ui red google button">
                    <i className="google icon" />
                    Sign Out
                </button>
            )
            
        }
        else {
            return (
                <button onClick={this.onSignInClick} className="ui red google button">
                    <i className="google icon" />
                    Sign In with Google
                </button>
            )
        }
    }

    render() {
        return <div>{this.renderAuthButton()}</div>
    }
}

const mapStateToProps = (state) => {
    return { isSignedIn: state.auth.isSignedIn };
}

export default connect(mapStateToProps, {signIn, signOut})(GoogleAuth);