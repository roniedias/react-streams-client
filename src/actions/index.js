import streams from '../apis/streams';
import history from '../history';

import { 
    SIGN_IN, 
    SIGN_OUT, 
    CREATE_STREAM,
    FETCH_STREAMS,
    FETCH_STREAM,
    DELETE_STREAM,
    EDIT_STREAM
} from './types';

export const signIn = (userId) => {
    return {
        type: SIGN_IN,
        payload: userId
    }
}

export const signOut = () => {
    return {
        type: SIGN_OUT
    }
}


/*
export const createStream = (formValues) => {
    return async (dispatch) => {
        streams.post('streams', formValues);
    }
};

is the same as...

export const createStream = formValues => async (dispatch) => {
    streams.post('streams', formValues);
}
*/

export const createStream = formValues => async (dispatch, getState) => { // second argument (getState) was added here so we can add the userId to the createStream method
    
    
    const { userId } = getState().auth; // Destructured: it will access the getState().auth (property) and pass only the userId value to const userId
    const response = await streams.post('/streams', { ...formValues, userId }); // Way of including userId into formValues

    dispatch({ type: CREATE_STREAM, payload: response.data });

    history.push('/'); // Programmatically navigating user back to the root route
}

export const fetchStreams = () => async dispatch => {
    const response = await streams.get('/streams');

    dispatch({ type: FETCH_STREAMS, payload: response.data });
}


export const fetchStream = (id) => async dispatch => {
    const response = await streams.get(`/streams/${id}`);

    dispatch({ type: FETCH_STREAM, payload: response.data });
}


export const editStream = (id, formValues) => async dispatch => {
    const response = await streams.patch(`/streams/${id}`, formValues);

    dispatch({ type: EDIT_STREAM, payload: response.data });

    history.push('/');
}


export const deleteStream = (id) => async dispatch => {
    await streams.delete(`/streams/${id}`);

    dispatch({ type: DELETE_STREAM, payload: id });

    history.push('/');
}

